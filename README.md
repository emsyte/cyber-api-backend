Tailored Labs API
=================

API heavily based on ["Express & ES6 REST API
Boilerplate"](https://github.com/developit/express-es6-rest-api).

Getting Started
---------------

```sh
# Start development live-reload server
npm run dev

# Start production server:
npm start
```


Configuration Details:
----------------------
API FQDN: api.tailoredlabs.org

Guacamole Port = 4822


License
-------
MIT
