//import sessions from '../models/sessions';
import { Router } from 'express';
import config from '../config.json';

const crypto = require('crypto');
const clientOptions = {
    cypher: process.env.GUAC_CRYPT_CYPHER || config.cryptCypher,
    key:    process.env.GUAC_CRYPT_KEY    || config.cryptKey
};

let api = Router();

let guacToken = (connectionSettings) => {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(clientOptions.cypher, clientOptions.key, iv);

    let crypted = cipher.update(JSON.stringify(connectionSettings), 'utf8', 'base64');
    crypted += cipher.final('base64');

    const data = {
        iv: iv.toString('base64'),
        value: crypted
    };

    return new Buffer(JSON.stringify(data)).toString('base64');
};

api.post('/', (req, res) => {

    let connectionSettings = {
        connection: {
          type: "vnc",
          settings: {
              hostname: "dummy-lesson-dummy-vnc-app.lessons",
              "enable-wallpaper": false
          }
        }
    };

    let token     = guacToken(connectionSettings),
        sessionId = '1'; // TODO: retrieve this from authorization headers

    const baseUrl = process.env.ENDPOINT_FQDN || config.endpointFQDN;
    const url = `wss://${baseUrl}/guacamole?token=${token}`;

    console.log('ERROR: THIS ENDPOINT IS A STUB');
    res.json({
        token,
        url,
        sessionId
    });
});

api.post('/:sessionId/still_active', (req, res) => {
    let sessionId = req.params.sessionId;

    console.log('ERROR: THIS ENDPOINT IS A STUB');
    res.json({status: 'OK'});
});

api.delete('/:sessionId', (req, res) => {
    let sessionId = req.params.sessionId;

    console.log('ERROR: THIS ENDPOINT IS A STUB');
    res.json({status: 'OK'});
});

export default api;
