import { version } from '../../package.json';
import { Router } from 'express';
import sessions from './sessions';

export default ({ config, db }) => {
	let api = Router();

  api.use('/sessions', sessions);

	api.get('/', (req, res) => {
		res.json({ version });
	});

  api.get('/status', (req, res) => {
      res.json({
          'status': 'OK',
          version
      });
  });

	return api;
}
