import config from '../config.json';

class Guac {

    constructor({ server }){
        const GuacamoleLite = require('guacamole-lite');
        const websocketOptions = {
            server,
            path: '/guacamole'
        };

        const guacdOptions = {
            host: process.env.GUAC_HOST || config.guacHost,
            port: process.env.GUAC_PORT || config.guacPort // Default Guacd port
        };

        const clientOptions = {
            crypt: {
                cypher: process.env.GUAC_CRYPT_CYPHER || config.cryptCypher,
                key:    process.env.GUAC_CRYPT_KEY    || config.cryptKey
            },
            allowedUnencryptedConnectionSettings: {
                rdp: [
                    'width',
                    'height',
                    'dpi',
                    'create-drive-path'
                ]
            }
        };
        return new GuacamoleLite(websocketOptions, guacdOptions, clientOptions);
    }

}

export default Guac;
